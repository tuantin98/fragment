package com.example.afinal.Fragment;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class ViewPaperFragment extends FragmentStatePagerAdapter {
    public ViewPaperFragment(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null ;
        switch (position){
            case 0 :  fragment = new AllTaskFragment() ;
            case 1 :  fragment = new DoneTaskFragment() ;
        }
        return fragment ;
    }

    @Override
    public int getCount() {
        return 2 ;
    }
    public CharSequence getPageTitle(int position) {
        String title = "";
        switch (position){
            case 0:
                title = "One";
                break;
            case 1:
                title = "Two";
                break;
            case 2:
                title = "Three";
                break;
        }
        return title;
    }
}
