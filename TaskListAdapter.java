package Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.vuongnguyen.R;
import java.util.List;

import Activity.RepairDataActivity;
import ModelObject.ListTask;

public class ListTaskAdapter extends RecyclerView.Adapter<ListTaskAdapter.ViewHolder> {

    private final OnToDoItemListener mListener;
    private Context mContext ;
    private List<ListTask> mList ;
    private boolean flag = false ;

    public ListTaskAdapter(Context mContext, List<ListTask> mList,OnToDoItemListener listener) {
        this.mContext = mContext;
        this.mList = mList;
        this.mListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, final int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.complete_checbox,parent,false);
        final ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.mLayoutInfoTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onClickItem(viewHolder.getAdapterPosition());
            }
        });
        viewHolder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onClockDeleteItem(viewHolder.getAdapterPosition());
            }
        });
        viewHolder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mListener.onCheckboxClick(viewHolder.getAdapterPosition(),isChecked);
            }
        });
        return viewHolder;
    }


    // Code trigger event completed, delete, update task in here
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.mTxtTitle.setText(mList.get(position).getTitle());
        holder.mTxtInfo.setText(mList.get(position).getInfo());
        holder.mTxtID.setText(mList.get(position).getId());
        //set checked
        flag = mList.get(position).getCompleted() != 0;
        if(!flag){
            holder.mCheckBox.setChecked(false);
        } else {
            holder.mCheckBox.setChecked(true);
        }
    }
    @Override
    public int getItemCount() {
        return mList.size();
    }

    public  class  ViewHolder extends RecyclerView.ViewHolder{
        CheckBox mCheckBox ;
        TextView mTxtTitle ;
        TextView mTxtInfo;
        LinearLayout mLayoutInfoTask;
        TextView mTxtID;
        LinearLayout mLayoutDelete;
        ImageButton deleteButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mCheckBox = itemView.findViewById(R.id.cb_complete);
            mTxtTitle = itemView.findViewById(R.id.txt_completed);
            mTxtInfo = itemView.findViewById(R.id.text_info);
            mLayoutInfoTask = itemView.findViewById(R.id.layout_info_task);
            mTxtID = itemView.findViewById(R.id.txt_id);
            mLayoutDelete = itemView.findViewById(R.id.layout_delete);
            deleteButton = itemView.findViewById(R.id.img_button_delete);
        }
    }
    public interface OnToDoItemListener{
        void onClickItem(int index);//Click on an item
        void onClockDeleteItem(int index);//Click on an item for deleting.
        void onCheckboxClick(int index,boolean status);//..
    }
}
