package com.example.afinal;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.example.afinal.Fragment.ViewPaperFragment;
import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {
    private Toolbar mToolbar ;
    private TabLayout mTbLayout ;
    private ViewPager mViewPaper ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mapping();
        FragmentManager fragmentManager = getSupportFragmentManager();
        ViewPaperFragment adapter = new ViewPaperFragment(fragmentManager) ;
        mViewPaper.setAdapter(adapter);
        mTbLayout.setupWithViewPager(mViewPaper);

    }

    void mapping(){
        mToolbar = findViewById(R.id.tb_menu);
        mTbLayout = findViewById(R.id.tb_data);
        mViewPaper = findViewById(R.id.view_paper) ;
    }
}
